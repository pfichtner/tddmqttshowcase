package com.github.pfichtner.tddmqttshowcase;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.github.pfichtner.tddmqttshowcase.MessageCollector;
import com.github.pfichtner.tddmqttshowcase.TimedOutException;

public class MessageCollectorTest {

	private static final int TEN_SECONDS_AS_MILLIS = 10 * 1000;

	@Test(timeout = TEN_SECONDS_AS_MILLIS, expected = TimedOutException.class)
	public void throwsExceptionWhenNoMessageReceivedWithinTimeout()
			throws TimedOutException {
		MessageCollector.newInstance(new TestMessageGenerator()).nextMessage(5,
				MILLISECONDS);
	}

	@Test(timeout = TEN_SECONDS_AS_MILLIS)
	public void returnsMessageWhenMessageIsReceivedBeforeAccesed()
			throws TimedOutException {
		MessageCollector<String> collector = MessageCollector
				.newInstance(new TestMessageGenerator("foo"));
		assertThat(collector.nextMessage(1, MINUTES), is("foo"));
	}

	@Test(timeout = TEN_SECONDS_AS_MILLIS)
	// TODO this test does not really guarantee that #messageReceived is called
	// after #waitForMessage; (check for
	// com.googlecode.thread-weaver:threadweaver to really solve this)
	public void returnsMessageWhenMessageIsReceivedAfterAccesesStarted()
			throws TimedOutException {
		final TestMessageGenerator mg = new TestMessageGenerator();
		MessageCollector<String> collector = MessageCollector.newInstance(mg);

		new Thread(new Runnable() {
			public void run() {
				try {
					SECONDS.sleep(1);
					mg.generateMessage("bar");
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		}).start();

		assertThat(
				collector.nextMessage(TEN_SECONDS_AS_MILLIS * 2, MILLISECONDS),
				is("bar"));
	}

}
