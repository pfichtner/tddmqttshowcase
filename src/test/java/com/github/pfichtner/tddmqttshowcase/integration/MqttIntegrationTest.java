package com.github.pfichtner.tddmqttshowcase.integration;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import io.moquette.server.Server;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.junit.Test;

import com.github.pfichtner.tddmqttshowcase.MessageCollector;
import com.github.pfichtner.tddmqttshowcase.TimedOutException;
import com.github.pfichtner.tddmqttshowcase.integration.MqttAdapter.MqttConfig;
import com.github.pfichtner.tddmqttshowcase.integration.MqttAdapter.TopicedMessage;

public class MqttIntegrationTest {

	@Test
	public void doesReceiveMessagesPublishedByMqttClient()
			throws TimedOutException, MqttException, IOException,
			InterruptedException {

		MqttConfig config = new MqttConfig("localhost", getFreeLocalPort());

		startBroker(config);

		// we do not subscribe on topic3
		MessageCollector<TopicedMessage> collector = MessageCollector
				.newInstance(new MqttAdapter(config).subscribe("topic1/+")
						.subscribe("topic2/+").subscribe("topic4/+")
						.subscribe("topic5/+"));

		List<TopicedMessage> toPublish = messages(10);
		publishMessagesInBackground(config, toPublish);

		List<TopicedMessage> expected = new ArrayList<TopicedMessage>(toPublish);
		expected.remove(new TopicedMessage("topic3", "message3".getBytes()));
		expected.remove(new TopicedMessage("topic3", "message8".getBytes()));

		assertThat(expected, is(collect(collector, expected.size())));
	}

	private List<TopicedMessage> collect(
			MessageCollector<TopicedMessage> collector, int amount)
			throws TimedOutException {
		List<TopicedMessage> messages = new ArrayList<TopicedMessage>(amount);
		for (int i = 0; i < amount; i++) {
			messages.add(collector.nextMessage(1, SECONDS));
		}
		return messages;
	}

	private List<TopicedMessage> messages(int maxMessages) {
		int tCounter = 0;
		int mCounter = 0;
		List<TopicedMessage> messages = new ArrayList<TopicedMessage>();
		for (int i = 0; i < maxMessages; i++) {
			if (++tCounter > 5) {
				tCounter = 1;
			}
			messages.add(new TopicedMessage("topic" + tCounter,
					("message" + ++mCounter).getBytes()));
		}
		return messages;
	}

	private int getFreeLocalPort() throws IOException {
		ServerSocket serverSocket = new ServerSocket(0);
		try {
			return serverSocket.getLocalPort();
		} finally {
			serverSocket.close();
		}
	}

	private Server startBroker(MqttConfig config) throws IOException {
		Server broker = new Server();
		broker.startServer(toMoquetteProperties(config));
		return broker;
	}

	private Properties toMoquetteProperties(MqttConfig config) {
		Properties p = new Properties();
		p.put("host", config.getHost());
		p.put("port", String.valueOf(config.getPort()));
		p.put("password_file", "config/password_file.conf");
		return p;
	}

	private void publishMessagesInBackground(MqttConfig config,
			final List<TopicedMessage> toPublish) throws InterruptedException,
			MqttException {

		final MqttClient client = new MqttClient(MqttAdapter.uri(config),
				"messagePublisher");
		client.connect();

		new Thread() {
			{
				setDaemon(true);
				start();
			}

			@Override
			public void run() {
				try {
					for (TopicedMessage message : toPublish) {
						client.publish(message.getTopic(),
								message.getPayload(), 0, false);
					}
					client.disconnect();
				} catch (MqttPersistenceException e) {
					throw new RuntimeException(e);
				} catch (MqttException e) {
					throw new RuntimeException(e);
				}
			}
		};
	}

}
