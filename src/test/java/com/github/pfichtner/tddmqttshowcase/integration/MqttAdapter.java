package com.github.pfichtner.tddmqttshowcase.integration;

import java.util.Arrays;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.github.pfichtner.tddmqttshowcase.AbstractAdapter;
import com.github.pfichtner.tddmqttshowcase.integration.MqttAdapter.TopicedMessage;

public class MqttAdapter extends AbstractAdapter<TopicedMessage> {

	public static class MqttConfig {

		private final String host;
		private final int port;

		public MqttConfig(String host, int port) {
			this.host = host;
			this.port = port;
		}

		public String getHost() {
			return host;
		}

		public int getPort() {
			return port;
		}

	}

	public static class TopicedMessage {

		private final String topic;
		private final byte[] payload;

		public TopicedMessage(String topic, byte[] payload) {
			this.topic = topic;
			this.payload = payload;
		}

		public String getTopic() {
			return topic;
		}

		public byte[] getPayload() {
			return payload;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + Arrays.hashCode(payload);
			result = prime * result + ((topic == null) ? 0 : topic.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TopicedMessage other = (TopicedMessage) obj;
			if (!Arrays.equals(payload, other.payload))
				return false;
			if (topic == null) {
				if (other.topic != null)
					return false;
			} else if (!topic.equals(other.topic))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "TopicedMessage [topic=" + topic + ", payload="
					+ new String(payload) + "]";
		}

	}

	private final MqttClient delegate;

	public MqttAdapter(MqttConfig config) throws MqttException {
		this.delegate = new MqttClient(uri(config), "messageCollector");
		this.delegate.connect();
		this.delegate.setCallback(new MqttCallback() {

			public void messageArrived(String topic, MqttMessage message) {
				MqttAdapter.this.messageReceived(new TopicedMessage(topic,
						message.getPayload()));
			}

			public void deliveryComplete(IMqttDeliveryToken mqttDeliveryToken) {
			}

			public void connectionLost(Throwable t) {
				// TODO write a failing test and add reconnect code

			}
		});
	}

	public static String uri(MqttConfig config) {
		return "tcp://" + config.getHost() + ":" + config.getPort();
	}

	public MqttAdapter subscribe(String topic) throws MqttException {
		this.delegate.subscribe(topic);
		return this;
	}

}