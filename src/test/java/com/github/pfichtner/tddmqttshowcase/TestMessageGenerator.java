package com.github.pfichtner.tddmqttshowcase;

import java.util.Arrays;
import java.util.List;

public class TestMessageGenerator extends AbstractAdapter<String> {

	private List<String> initialMessages;

	public TestMessageGenerator() {
		super();
	}

	public TestMessageGenerator(String... messages) {
		this.initialMessages = Arrays.asList(messages);
	}

	@Override
	public void register(MessageCollector<String> messageCollector) {
		super.register(messageCollector);
		if (this.initialMessages != null) {
			for (String message : this.initialMessages) {
				generateMessage(message);
			}
		}
	}

	public void generateMessage(String message) {
		messageReceived(message);
	}
}