package com.github.pfichtner.tddmqttshowcase;

public interface Adapter<T> {

	void register(MessageCollector<T> messageCollector);

}
