package com.github.pfichtner.tddmqttshowcase;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

public abstract class MessageCollector<T> {

	public abstract T nextMessage(int timeout, TimeUnit timeUnit)
			throws TimedOutException;

	public abstract void messageReceived(T message);

	public static <T> MessageCollector<T> newInstance(Adapter<T> adapter) {
		MessageCollector<T> messageCollector = new MessageCollector<T>() {

			private final BlockingQueue<T> messages = new ArrayBlockingQueue<T>(
					1024);

			@Override
			public T nextMessage(int timeout, TimeUnit timeUnit)
					throws TimedOutException {
				try {
					T polled = this.messages.poll(timeout, timeUnit);
					if (polled == null) {
						throw new TimedOutException();
					}
					return polled;
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}

			@Override
			public void messageReceived(T message) {
				try {
					this.messages.put(message);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}

		};
		adapter.register(messageCollector);
		return messageCollector;
	}

}
