package com.github.pfichtner.tddmqttshowcase;


public abstract class AbstractAdapter<T> implements Adapter<T> {

	private MessageCollector<T> messageCollector;

	public void register(MessageCollector<T> messageCollector) {
		this.messageCollector = messageCollector;
	}

	public void messageReceived(T message) {
		this.messageCollector.messageReceived(message);
	}

}
