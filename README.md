# tddmqttshowcase [![pipeline status](https://gitlab.com/pfichtner/tddmqttshowcase/badges/master/pipeline.svg)](https://gitlab.com/pfichtner/tddmqttshowcase/commits/master)

Wait for a mqtt message. If the message is not received within a given timeout a TimeoutException is thrown

Originally I thought to need this functionality inside one of my programs but I was wrong. 
Because it's a nice showcase for [TDD](https://en.wikipedia.org/wiki/Test-driven_development) I decided to commit anyway. 

Actual  it seems a high dependency on mqtt and timeouts for the requirement but the tests show that you don't need 
these systems nor mocks of them and how nice and flexible the design goes if you're doing [TDD](https://en.wikipedia.org/wiki/Test-driven_development). 

